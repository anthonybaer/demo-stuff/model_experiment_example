## Difference between APIs
{%- for endpoint in endpoints %}

### {{ endpoint.method}} {{endpoint.path}}
{%- for case in endpoint.cases %}

#### {{ case.title }}

{%- if case.GITLAB %}
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>{{ case.MLFLOW.url }}</td><td>{{ case.GITLAB.url }}</td></tr>
<tr><td>Params</td><td>{{ case.MLFLOW.params }}</td><td>{{ case.GITLAB.params }}</td></tr>
<tr><td>Body</td><td>{{ case.MLFLOW.request_body }}</td><td>{{ case.GITLAB.request_body }}</td></tr>
<tr><td>Status Code</td><td>{{ case.MLFLOW.status_code }}</td><td>{{ case.GITLAB.status_code }}</td></tr>
<tr><td>Reponse</td><td>{{ case.MLFLOW.response_body }}</td><td> {{ case.GITLAB.response_body }}</td></tr>
</table>
{%- else %}
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>{{ case.MLFLOW.url }}</td></tr>
<tr><td>Params</td><td>{{ case.MLFLOW.params }}</td></tr>
<tr><td>Body</td><td>{{ case.MLFLOW.request_body }}</td></tr>
<tr><td>Status Code</td><td>{{ case.MLFLOW.status_code }}</td></tr>
<tr><td>Reponse</td><td>{{ case.MLFLOW.response_body }}</td></tr>
</table>
{%- endif %}
{%- endfor %}
{%- endfor %}