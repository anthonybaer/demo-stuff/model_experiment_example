## Difference between APIs

### POST /experiments/create

#### When name does not yet exist
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/experiments/create</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'name': '4ddae346-a6bf-4a77-b012-b0c3c86ba448'}</td></tr>
<tr><td>Status Code</td><td>200</td></tr>
<tr><td>Reponse</td><td>{
  "experiment_id": "1"
}</td></tr>
</table>

#### When name already exists
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/experiments/create</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'name': '4ddae346-a6bf-4a77-b012-b0c3c86ba448'}</td></tr>
<tr><td>Status Code</td><td>400</td></tr>
<tr><td>Reponse</td><td>b'{"error_code": "RESOURCE_ALREADY_EXISTS", "message": "Experiment(name=4ddae346-a6bf-4a77-b012-b0c3c86ba448) already exists. Error: (raised as a result of Query-invoked autoflush; consider using a session.no_autoflush block if this flush is occurring prematurely)\\n(sqlite3.IntegrityError) UNIQUE constraint failed: experiments.name\\n[SQL: INSERT INTO experiments (name, artifact_location, lifecycle_stage) VALUES (?, ?, ?)]\\n[parameters: (\'4ddae346-a6bf-4a77-b012-b0c3c86ba448\', \'\', \'active\')]\\n(Background on this error at: https://sqlalche.me/e/14/gkpj)"}'</td></tr>
</table>

#### When name is missing
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/experiments/create</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'yolo': 'experiment_name'}</td></tr>
<tr><td>Status Code</td><td>400</td></tr>
<tr><td>Reponse</td><td>b'{"error_code": "INVALID_PARAMETER_VALUE", "message": "Missing value for required parameter \'name\'. See the API docs for more information about request parameters."}'</td></tr>
</table>

### GET /experiments/get

#### When id exists
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/experiments/get</td></tr>
<tr><td>Params</td><td>{'experiment_id': '1'}</td></tr>
<tr><td>Body</td><td>{}</td></tr>
<tr><td>Status Code</td><td>200</td></tr>
<tr><td>Reponse</td><td>{
  "experiment": {
    "experiment_id": "1",
    "name": "4ddae346-a6bf-4a77-b012-b0c3c86ba448",
    "artifact_location": "./mlruns/1",
    "lifecycle_stage": "active"
  }
}</td></tr>
</table>

#### When id does not exist
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/experiments/get</td></tr>
<tr><td>Params</td><td>{'experiment_id': 'asasdfsadf'}</td></tr>
<tr><td>Body</td><td>{}</td></tr>
<tr><td>Status Code</td><td>404</td></tr>
<tr><td>Reponse</td><td>b'{"error_code": "RESOURCE_DOES_NOT_EXIST", "message": "No Experiment with id=asasdfsadf exists"}'</td></tr>
</table>

#### When id is missing
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/experiments/get</td></tr>
<tr><td>Params</td><td>{'asdasd': 'asasdfsadf'}</td></tr>
<tr><td>Body</td><td>{}</td></tr>
<tr><td>Status Code</td><td>200</td></tr>
<tr><td>Reponse</td><td>{
  "experiment": {
    "experiment_id": "0",
    "name": "Default",
    "artifact_location": "./mlruns/0",
    "lifecycle_stage": "active"
  }
}</td></tr>
</table>

### GET /experiments/get-by-name

#### When name exists
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/experiments/get-by-name</td></tr>
<tr><td>Params</td><td>{'experiment_name': '4ddae346-a6bf-4a77-b012-b0c3c86ba448'}</td></tr>
<tr><td>Body</td><td>{}</td></tr>
<tr><td>Status Code</td><td>200</td></tr>
<tr><td>Reponse</td><td>{
  "experiment": {
    "experiment_id": "1",
    "name": "4ddae346-a6bf-4a77-b012-b0c3c86ba448",
    "artifact_location": "./mlruns/1",
    "lifecycle_stage": "active"
  }
}</td></tr>
</table>

#### When name does not exist
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/experiments/get-by-name</td></tr>
<tr><td>Params</td><td>{'experiment_name': 'asasdfsadf'}</td></tr>
<tr><td>Body</td><td>{}</td></tr>
<tr><td>Status Code</td><td>404</td></tr>
<tr><td>Reponse</td><td>b'{"error_code": "RESOURCE_DOES_NOT_EXIST", "message": "Could not find experiment with name \'asasdfsadf\'"}'</td></tr>
</table>

#### When name is missing
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/experiments/get-by-name</td></tr>
<tr><td>Params</td><td>{'asdasd': 'asasdfsadf'}</td></tr>
<tr><td>Body</td><td>{}</td></tr>
<tr><td>Status Code</td><td>404</td></tr>
<tr><td>Reponse</td><td>b'{"error_code": "RESOURCE_DOES_NOT_EXIST", "message": "Could not find experiment with name \'\'"}'</td></tr>
</table>

### POST /runs/create

#### When experiment exists
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/create</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'experiment_id': '1', 'start_time': 1234}</td></tr>
<tr><td>Status Code</td><td>200</td></tr>
<tr><td>Reponse</td><td>{
  "run": {
    "info": {
      "run_uuid": "2b3fc5a0bce34bc58d260e6b2dad00b6",
      "experiment_id": "1",
      "user_id": "",
      "status": "RUNNING",
      "start_time": 1234,
      "artifact_uri": "./mlruns/1/2b3fc5a0bce34bc58d260e6b2dad00b6/artifacts",
      "lifecycle_stage": "active",
      "run_id": "2b3fc5a0bce34bc58d260e6b2dad00b6"
    },
    "data": {}
  }
}</td></tr>
</table>

#### When experiment does not exist
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/create</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'experiment_id': 'asasdfsadf'}</td></tr>
<tr><td>Status Code</td><td>404</td></tr>
<tr><td>Reponse</td><td>b'{"error_code": "RESOURCE_DOES_NOT_EXIST", "message": "No Experiment with id=asasdfsadf exists"}'</td></tr>
</table>

#### When experiment is not passed
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/create</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'yolo': 'asasdfsadf'}</td></tr>
<tr><td>Status Code</td><td>400</td></tr>
<tr><td>Reponse</td><td>b'{"error_code": "BAD_REQUEST", "message": "(sqlite3.IntegrityError) FOREIGN KEY constraint failed\\n[SQL: INSERT INTO runs (run_uuid, name, source_type, source_name, entry_point_name, user_id, status, start_time, end_time, source_version, lifecycle_stage, artifact_uri, experiment_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)]\\n[parameters: (\'67542435b96c4269820eca8759bb6081\', \'\', \'UNKNOWN\', \'\', \'\', \'\', \'RUNNING\', 0, None, \'\', \'active\', \'./mlruns/0/67542435b96c4269820eca8759bb6081/artifacts\', \'\')]\\n(Background on this error at: https://sqlalche.me/e/14/gkpj)"}'</td></tr>
</table>

### GET /runs/get

#### When run exists
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/get</td></tr>
<tr><td>Params</td><td>{'run_id': '2b3fc5a0bce34bc58d260e6b2dad00b6'}</td></tr>
<tr><td>Body</td><td>{}</td></tr>
<tr><td>Status Code</td><td>200</td></tr>
<tr><td>Reponse</td><td>{
  "run": {
    "info": {
      "run_uuid": "2b3fc5a0bce34bc58d260e6b2dad00b6",
      "experiment_id": "1",
      "user_id": "",
      "status": "RUNNING",
      "start_time": 1234,
      "artifact_uri": "./mlruns/1/2b3fc5a0bce34bc58d260e6b2dad00b6/artifacts",
      "lifecycle_stage": "active",
      "run_id": "2b3fc5a0bce34bc58d260e6b2dad00b6"
    },
    "data": {}
  }
}</td></tr>
</table>

#### When run does not exist
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/get</td></tr>
<tr><td>Params</td><td>{'run_id': 'asasdfsadf'}</td></tr>
<tr><td>Body</td><td>{}</td></tr>
<tr><td>Status Code</td><td>404</td></tr>
<tr><td>Reponse</td><td>b'{"error_code": "RESOURCE_DOES_NOT_EXIST", "message": "Run with id=asasdfsadf not found"}'</td></tr>
</table>

### POST /runs/update

#### When run exists
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/update</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'run_id': '2b3fc5a0bce34bc58d260e6b2dad00b6', 'status': 'FAILED', 'end_time': 12345678}</td></tr>
<tr><td>Status Code</td><td>200</td></tr>
<tr><td>Reponse</td><td>{
  "run_info": {
    "run_uuid": "2b3fc5a0bce34bc58d260e6b2dad00b6",
    "experiment_id": "1",
    "user_id": "",
    "status": "FAILED",
    "start_time": 1234,
    "end_time": 12345678,
    "artifact_uri": "./mlruns/1/2b3fc5a0bce34bc58d260e6b2dad00b6/artifacts",
    "lifecycle_stage": "active",
    "run_id": "2b3fc5a0bce34bc58d260e6b2dad00b6"
  }
}</td></tr>
</table>

#### When run does not exist
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/update</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'run_id': 'asasdfsadf'}</td></tr>
<tr><td>Status Code</td><td>404</td></tr>
<tr><td>Reponse</td><td>b'{"error_code": "RESOURCE_DOES_NOT_EXIST", "message": "Run with id=asasdfsadf not found"}'</td></tr>
</table>

#### When run exists, but state is invalid
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/update</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'run_id': '2b3fc5a0bce34bc58d260e6b2dad00b6', 'status': 'YOLO'}</td></tr>
<tr><td>Status Code</td><td>200</td></tr>
<tr><td>Reponse</td><td>{
  "run_info": {
    "run_uuid": "2b3fc5a0bce34bc58d260e6b2dad00b6",
    "experiment_id": "1",
    "user_id": "",
    "status": "RUNNING",
    "start_time": 1234,
    "artifact_uri": "./mlruns/1/2b3fc5a0bce34bc58d260e6b2dad00b6/artifacts",
    "lifecycle_stage": "active",
    "run_id": "2b3fc5a0bce34bc58d260e6b2dad00b6"
  }
}</td></tr>
</table>

### POST /runs/log-metric

#### When run exists
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/log-metric</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'run_id': '2b3fc5a0bce34bc58d260e6b2dad00b6', 'key': 'hello', 'value': 10.0, 'timestamp': 12345678, 'step': 3}</td></tr>
<tr><td>Status Code</td><td>200</td></tr>
<tr><td>Reponse</td><td>{}</td></tr>
</table>

#### When run id is not passed
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/log-metric</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'key': 'hello', 'value': 10.0, 'timestamp': 12345678, 'step': 3}</td></tr>
<tr><td>Status Code</td><td>400</td></tr>
<tr><td>Reponse</td><td>b'{"error_code": "INVALID_PARAMETER_VALUE", "message": "Missing value for required parameter \'run_id\'. See the API docs for more information about request parameters."}'</td></tr>
</table>

#### When key is not passed
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/log-metric</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'run_id': '2b3fc5a0bce34bc58d260e6b2dad00b6', 'value': 10.0, 'timestamp': 12345678, 'step': 3}</td></tr>
<tr><td>Status Code</td><td>400</td></tr>
<tr><td>Reponse</td><td>b'{"error_code": "INVALID_PARAMETER_VALUE", "message": "Missing value for required parameter \'key\'. See the API docs for more information about request parameters."}'</td></tr>
</table>

### POST /runs/log-parameter

#### When run exists
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/log-parameter</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'run_id': '2b3fc5a0bce34bc58d260e6b2dad00b6', 'key': 'hello', 'value': 'SomeParameter'}</td></tr>
<tr><td>Status Code</td><td>200</td></tr>
<tr><td>Reponse</td><td>{}</td></tr>
</table>

#### When key is not passed
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/log-parameter</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'run_id': '2b3fc5a0bce34bc58d260e6b2dad00b6', 'value': 'SomeParameter'}</td></tr>
<tr><td>Status Code</td><td>400</td></tr>
<tr><td>Reponse</td><td>b'{"error_code": "INVALID_PARAMETER_VALUE", "message": "Missing value for required parameter \'key\'. See the API docs for more information about request parameters."}'</td></tr>
</table>

#### When param is repeated
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/log-parameter</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'run_id': '2b3fc5a0bce34bc58d260e6b2dad00b6', 'key': 'hello', 'value': 'SomeParameter'}</td></tr>
<tr><td>Status Code</td><td>200</td></tr>
<tr><td>Reponse</td><td>{}</td></tr>
</table>

### POST /runs/log-batch

#### When run exists
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/log-batch</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'run_id': '2b3fc5a0bce34bc58d260e6b2dad00b6', 'metrics': [{'key': 'metric2', 'value': 1.0, 'timestamp': 12345678, 'step': 0}, {'key': 'metric3', 'value': 100.0, 'timestamp': 12345678, 'step': 0}], 'params': [{'key': 'param1', 'value': 'ValueForParam1'}]}</td></tr>
<tr><td>Status Code</td><td>200</td></tr>
<tr><td>Reponse</td><td>{}</td></tr>
</table>

#### When a metric is passed without key
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/log-batch</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'run_id': '2b3fc5a0bce34bc58d260e6b2dad00b6', 'metrics': [{'value': 1.0, 'timestamp': 12345678, 'step': 0}, {'key': 'metric4', 'value': 100.0, 'timestamp': 12345678, 'step': 0}]}</td></tr>
<tr><td>Status Code</td><td>500</td></tr>
<tr><td>Reponse</td><td>b'<!doctype html>\n<html lang=en>\n<title>500 Internal Server Error</title>\n<h1>Internal Server Error</h1>\n<p>The server encountered an internal error and was unable to complete your request. Either the server is overloaded or there is an error in the application.</p>\n'</td></tr>
</table>

#### When a parameter is duplicated
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/log-batch</td></tr>
<tr><td>Params</td><td>{}</td></tr>
<tr><td>Body</td><td>{'run_id': '2b3fc5a0bce34bc58d260e6b2dad00b6', 'params': [{'key': 'param1', 'value': 'AnotherValue'}, {'key': 'param12', 'value': 'ValueForParam2'}]}</td></tr>
<tr><td>Status Code</td><td>400</td></tr>
<tr><td>Reponse</td><td>b'{"error_code": "INVALID_PARAMETER_VALUE", "message": "Changing param values is not allowed. Params were already logged=\'[{\'key\': \'param1\', \'old_value\': \'ValueForParam1\', \'new_value\': \'AnotherValue\'}, {\'key\': \'param12\', \'old_value\': None, \'new_value\': \'ValueForParam2\'}]\' for run ID=\'2b3fc5a0bce34bc58d260e6b2dad00b6\'."}'</td></tr>
</table>

### GET /runs/get

#### When run exists
<table>
<tr><td></td><td> Mlflow </td> <td> Gitlab </td></tr>
<tr><td>URL</td><td>http://127.0.0.1:5000/api/2.0/mlflow/runs/get</td></tr>
<tr><td>Params</td><td>{'run_id': '2b3fc5a0bce34bc58d260e6b2dad00b6'}</td></tr>
<tr><td>Body</td><td>{}</td></tr>
<tr><td>Status Code</td><td>200</td></tr>
<tr><td>Reponse</td><td>{
  "run": {
    "info": {
      "run_uuid": "2b3fc5a0bce34bc58d260e6b2dad00b6",
      "experiment_id": "1",
      "user_id": "",
      "status": "RUNNING",
      "start_time": 1234,
      "artifact_uri": "./mlruns/1/2b3fc5a0bce34bc58d260e6b2dad00b6/artifacts",
      "lifecycle_stage": "active",
      "run_id": "2b3fc5a0bce34bc58d260e6b2dad00b6"
    },
    "data": {
      "metrics": [
        {
          "key": "hello",
          "value": 10.0,
          "timestamp": 12345678,
          "step": 3
        },
        {
          "key": "metric2",
          "value": 1.0,
          "timestamp": 12345678,
          "step": 0
        },
        {
          "key": "metric3",
          "value": 100.0,
          "timestamp": 12345678,
          "step": 0
        }
      ],
      "params": [
        {
          "key": "hello",
          "value": "SomeParameter"
        },
        {
          "key": "param1",
          "value": "ValueForParam1"
        }
      ]
    }
  }
}</td></tr>
</table>